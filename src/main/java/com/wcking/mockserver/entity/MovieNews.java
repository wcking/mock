package com.wcking.mockserver.entity;

import lombok.Data;

import java.util.List;

@Data
public class MovieNews {

    private String title;
    private String imgSrc;
    private String desc;
    private int viewCount;
    private int playCount;
    /**
     * 0：左上小图
     * 1：左上大图
     * 2：左下
     * 3：右
     */
    private int type;
    private List<String> tags;

}
