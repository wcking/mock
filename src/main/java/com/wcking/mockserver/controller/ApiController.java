package com.wcking.mockserver.controller;

import com.wcking.mockserver.entity.*;
import com.wcking.mockserver.service.ApiService;
import com.wcking.mockserver.service.BillService;
import com.wcking.mockserver.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    UserService userService;

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public BaseResult login(@RequestBody LoginBody login) {
        if (Objects.isNull(login)) {
            return BaseResult.fail(ErrorEnum.PARAM_ERROR.getCode(), ErrorEnum.PARAM_ERROR.getMsg());

        }
        if (StringUtils.isEmpty(login.getLoginName()) || StringUtils.isEmpty(login.getPwd())) {
            return BaseResult.fail(ErrorEnum.PARAM_ERROR.getCode(), ErrorEnum.PARAM_ERROR.getMsg());

        }
        return userService.login(login.getLoginName(), login.getPwd());
    }

    @ApiOperation(value = "是否登录")
    @PostMapping("/isLogin")
    public BaseResult isLogin(@RequestParam("loginName") String loginName,
                              @RequestParam("token") String token) {
        if (StringUtils.isEmpty(loginName) || StringUtils.isEmpty(token)) {
            return BaseResult.fail(ErrorEnum.PARAM_ERROR.getCode(), ErrorEnum.PARAM_ERROR.getMsg());
        }
        if (userService.isLogin(loginName, token)) {
            return BaseResult.success(true);
        }
        return BaseResult.fail(ErrorEnum.NO_LOGIN_ERROR.getCode(), ErrorEnum.NO_LOGIN_ERROR.getMsg());
    }

    @Autowired
    BillService billService;

    @ApiOperation(value = "获取账单")
    @GetMapping("/getBill")
    public BaseResult getBill(@RequestParam("loginName") String loginName,
                              @RequestParam("token") String token) {
        if (!checkLogin(loginName, token)) {
            return BaseResult.fail(ErrorEnum.NO_LOGIN_ERROR.getCode(), ErrorEnum.NO_LOGIN_ERROR.getMsg());

        }
        return BaseResult.success(billService.getBillByMonth());
    }

    @Autowired
    ApiService apiService;

    @ApiOperation(value = "电影票座位信息")
    @GetMapping("/getTicket")
    public BaseResult<TicketData> getTicket(@RequestParam("loginName") String loginName,
                                            @RequestParam("token") String token) {
        if (!checkLogin(loginName, token)) {
            return BaseResult.fail(ErrorEnum.NO_LOGIN_ERROR.getCode(), ErrorEnum.NO_LOGIN_ERROR.getMsg());

        }

        TicketData data = apiService.getTicket();
        if (Objects.isNull(data)) {
            return BaseResult.fail(ErrorEnum.NO_MOVIE_ERROR.getCode(), ErrorEnum.NO_MOVIE_ERROR.getMsg());
        }
        return BaseResult.success(data);

    }

//    @ApiOperation(value = "电影列表")
//    @GetMapping("/movieList/{type}")
    public BaseResult<List<MovieData>> getMovies (@PathVariable("type") String type) {
        if (StringUtils.isEmpty(type)) {
            return BaseResult.fail(ErrorEnum.PARAM_ERROR.getCode(), ErrorEnum.PARAM_ERROR.getMsg());
        }
        List<MovieData> data = apiService.getMovie(type);
        if (Objects.isNull(data)) {
            return BaseResult.fail(ErrorEnum.PARAM_ERROR.getCode(), ErrorEnum.PARAM_ERROR.getMsg());

        }
        return BaseResult.success(data);
    }

    @ApiOperation(value = "电脑端电影列表")
    @GetMapping("/pcMovieRec")
    public BaseResult getPcRec(@RequestParam("loginName") String loginName,
                               @RequestParam("token") String token) {
        if (!checkLogin(loginName, token)) {
            return BaseResult.fail(ErrorEnum.NO_LOGIN_ERROR.getCode(), ErrorEnum.NO_LOGIN_ERROR.getMsg());

        }
        return BaseResult.success(apiService.getMovie("pcRec"));
    }


    @ApiOperation(value = "热点页面数据")
    @GetMapping("/pcNews/{type}")
    public BaseResult pcNews(@RequestParam("loginName") String loginName,
                             @RequestParam("token") String token,
                             @PathVariable("type") String type) {
        if (!checkLogin(loginName, token)) {
            return BaseResult.fail(ErrorEnum.NO_LOGIN_ERROR.getCode(), ErrorEnum.NO_LOGIN_ERROR.getMsg());

        }
        return BaseResult.success(apiService.getNews(type));
    }


    boolean checkLogin(String name, String token) {
        return userService.isLogin(name, token);
    }

 }
