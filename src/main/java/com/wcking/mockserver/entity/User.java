package com.wcking.mockserver.entity;

import lombok.Data;

@Data
public class User {
    private Long id;
    private String login_name;
    private String name;
    private String pwd;
    private String token;
}
