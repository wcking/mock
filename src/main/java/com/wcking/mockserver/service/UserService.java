package com.wcking.mockserver.service;

import com.wcking.mockserver.dao.UserMapper;
import com.wcking.mockserver.entity.BaseResult;
import com.wcking.mockserver.entity.ErrorEnum;
import com.wcking.mockserver.entity.User;
import com.wcking.mockserver.entity.UserEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UserService {

    private static final Map<String, String> userLoginMap = new ConcurrentHashMap<>();


    @Autowired
    UserMapper userMapper;

    public BaseResult<User> login(String loginName, String pwd) {
//        User byLogin = userMapper.findByLogin(loginName);
        UserEnum userEnum = UserEnum.USER_MAP.get(loginName);
        User byLogin = UserEnum.fromEnum(userEnum);
        if (Objects.isNull(byLogin)) {
            return BaseResult.fail(ErrorEnum.NO_USER_ERROR);
        }
        if (!pwd.equals(byLogin.getPwd())) {
            return BaseResult.fail(ErrorEnum.PWD_ERROR);
        }
        String token = UUID.randomUUID().toString();
        byLogin.setToken(token);
        userLoginMap.put(loginName, token);
        return BaseResult.success(byLogin);
    }

    public boolean isLogin(String loginName, String token) {
        String s = userLoginMap.get(loginName);
        if (StringUtils.isEmpty(s)) {
            return false;
        }
        if (s.equals(token)) {
            return true;
        }
        return false;
    }


}
