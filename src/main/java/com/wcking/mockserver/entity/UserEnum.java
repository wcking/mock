package com.wcking.mockserver.entity;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum UserEnum {

    ZHANGSAN(1L,"zhangsan","张三","a123456"),
    LISI(2L,"lisi","李四","b123456"),
    LIQIANG(3L,"liqiang","李强","c123456"),
    WANGHAI(4L,"wanghai","王海","d123456");


    public static final UserEnum[] ALL = {ZHANGSAN,LISI,LIQIANG,WANGHAI};
    public static final Map<String,UserEnum> USER_MAP = Arrays.stream(values()).collect(Collectors.toMap(UserEnum::getLogin_name, (e) -> {
        return e;
    }, (t1, t2) -> {
        return t2;
    }));

    public static User fromEnum(UserEnum userEnum) {
        if (userEnum == null) {
            return null;
        }
        User user = new User();
        user.setId(userEnum.getId());
        user.setName(userEnum.getName());
        user.setLogin_name(userEnum.getLogin_name());
        user.setPwd(userEnum.getPwd());
        return user;
    }

    private Long id;
    private String login_name;
    private String name;
    private String pwd;

    private UserEnum(Long id, String login_name, String name,String pwd ) {
        this.id = id;
        this.login_name = login_name;
        this.name = name;
        this. pwd = pwd;
    }

    public Long getId() {
        return id;
    }

    public String getLogin_name() {
        return login_name;
    }

    public String getName() {
        return name;
    }

    public String getPwd() {
        return pwd;
    }
}
