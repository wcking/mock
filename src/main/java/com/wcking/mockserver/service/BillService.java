package com.wcking.mockserver.service;

import com.wcking.mockserver.entity.BillData;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class BillService {

    public static final String[] DATE_STR = {"2021-01-10","2021-01-20","2021-02-10","2021-02-20",
            "2021-03-10","2021-03-20","2021-04-10","2021-04-20",
            "2021-05-10","2021-05-20","2021-06-10","2021-06-20",
            "2021-07-10","2021-07-20","2021-08-10","2021-08-20",
            "2021-09-10","2021-09-20","2021-10-10","2021-10-20",
            "2021-11-10","2021-11-20","2021-12-10","2021-12-20"};

    public static final Map<Integer, List<BillData>> data = new HashMap<>();
    public static List<BillData> all = new ArrayList<>();
    static Random random = new Random(1);

    static {
        init();
    }

    public List<BillData> getBillByMonth() {
        List<BillData> list = new ArrayList<>();
        Date cur = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(cur);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        for (int i =1; i <= 7; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 0 - i);
            Date date = calendar.getTime();
            String dateStr = format.format(date);
            BillData billData = new BillData();
            billData.setBillDate(dateStr);
            billData.setBillType(random.nextInt(6) + "");
            billData.setPaymentType(random.nextInt(2));
            billData.setBillMoney(random.nextInt(1000));
            list.add(billData);
            calendar.setTime(cur);
        }

        for (int i =8; i <= 30 ; i++) {
            calendar.add(Calendar.DATE, 0 - i);
            Date date = calendar.getTime();
            String dateStr = format.format(date);
            BillData billData = new BillData();
            billData.setBillDate(dateStr);
            billData.setBillType(random.nextInt(6) + "");
            billData.setPaymentType(random.nextInt(2));
            billData.setBillMoney(random.nextInt(1000));
            list.add(billData);
            calendar.setTime(cur);
        }

        for (int i = 31; i <= 11 * 30 ; i++) {
            if (i % 10 == 0) {
                calendar.add(Calendar.DATE, 0 - i);
                Date date = calendar.getTime();
                String dateStr = format.format(date);
                BillData billData = new BillData();
                billData.setBillDate(dateStr);
                billData.setBillType(random.nextInt(6) + "");
                billData.setPaymentType(random.nextInt(2));
                billData.setBillMoney(random.nextInt(1000));
                list.add(billData);
                calendar.setTime(cur);
            }
        }




        return list;
    }


    public static void init() {

        for (int i = 0; i <DATE_STR.length ; i++) {
            BillData billData = new BillData();
            billData.setBillDate(DATE_STR[i]);
            billData.setBillType(random.nextInt(6) + "");
            billData.setPaymentType(random.nextInt(2));
            all.add(billData);
        }
        Collections.sort(all);
    }

}
