package com.wcking.mockserver.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {


    @Bean
    public Docket customDocket() {
        ApiInfo swaggerApiInfo = new ApiInfoBuilder()
                .title("Welcome to  Swagger!")
                .build();
        ApiSelectorBuilder apiSelectorBuilder = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(swaggerApiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.wcking.mockserver.controller"));
        apiSelectorBuilder = apiSelectorBuilder.paths(PathSelectors.any());
        return apiSelectorBuilder.build();

    }
}
