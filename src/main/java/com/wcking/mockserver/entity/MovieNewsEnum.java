package com.wcking.mockserver.entity;

public enum MovieNewsEnum {

    TOP_1("《李茂换太子》曝“非常美丽”特辑 马丽化身侠女“暴揍”常远",
            "http://192.168.1.100:8080/pc/img/hot_1.jpg",
            "由高可执导并编剧，马丽、常远、艾伦、王成思、李海银等主演的跨年爆笑喜剧《李茂换太子》发布“非常美丽”特辑，解锁超多片场趣事。马丽饰演的杨家珍从小习武，丈夫李茂（常远 饰）对她百依百顺，常被家珍以“打是亲骂是爱”的方式百般疼爱，由此生出不少笑料。两人“戏里一家，戏外哈哈”，拍摄现场爆笑不断，而电影也将于12月31日将这份欢乐传递给大家。",
            200,
            160,1),
    TOP_2("《银行家》曝口碑特辑 九城超前观影获赞“商业版《绿皮书》”",
            "http://192.168.1.100:8080/pc/img/hot_2.jpg",
                 "由乔治·诺非执导，“猎鹰”安东尼·麦凯、“神盾局长”塞缪尔·杰克逊、“X战警”尼古拉斯·霍尔特主演的电影《银行家》今日发布口碑特辑和2张CINITY版海报。11月20日以来，电影在北京、上海、广州、成都、杭州等全国九城开展了超前观影活动，引发观影热潮，影片推广曲创作者黄小芸也出席了活动。第一批观众纷纷给予影片高口碑评价，认为影片“充满活力与温情，又不失对人性的思考”，是一部“值得走进影院看的电影”，更有观众称其为“‘商业版《绿皮书》’，商业谈判拍出了谍战片的感觉”。目前影片正在预售中，将于11月26日全国上映。",
            210,
                 180,0),
    TOP_3("打破多项纪录 电影《长津湖》登顶中国电影票房榜！",
            "http://192.168.1.100:8080/pc/img/hot_3.jpg",
        "作为一部史无前例的抗美援朝题材史诗巨制，电影《长津湖》冲破了疫情造成的种种困难，掀起了全民致敬先辈的热潮，形成了全社会范围的“长津湖效应”。为此，出品方博纳影业召集最强班底阵容，以最高工业化水准严格要求、倾情打造，确保影片有着过硬的质量和饱满的情感。影片上映后，形成的正面效应迅速扩散开来，全国电影观众纷纷走进影院，以实际行动表达对影片的喜爱、对历史的敬畏和对先辈的缅怀！这份成绩的背后，是中华儿女集体记忆、情感共振、家国情怀的激发和释放，伟大抗美援朝精神薪火相传，伟大英雄先辈永垂不朽！",
            210,
            180,0),
    BOTTOM_1("《平原上的火焰》创作特辑还原年代质感",
            "http://192.168.1.100:8080/pc/img/hot_4.jpg",
            "电影《平原上的火焰》今日发布“平原上的人”幕后创作特辑，不仅有监制刁亦男、导演张骥、艺术总监双雪涛等幕后主创揭秘拍摄故事，细节控剧组筹备5年，力求还原90年代质感；更有周冬雨、刘昊然等演员解读角色，多位实力派演员共同演绎两代人的复杂情感，影片将于12月24日全国上映。",
            221,
            100,2),
    BOTTOM_2("获奖无数《盲琴师》确认引进 男主传奇人生引全网热议”",
            "http://192.168.1.100:8080/pc/img/hot_5.jpg",
            "波兰电影《盲琴师》已确认引进，影片横扫欧洲各大电影节，创下16次提名8项获奖佳绩，同时还入围了上海电影节主竞赛单元角逐金爵奖，以及在金鸡国际影展上收获最受欢迎外国男演员奖。该片根据波兰天才盲人钢琴家米耶奇斯瓦夫·科什的真实故事改编，讲述他充满悲剧又传奇璀璨的一生。主演大卫·奥格尼克用注入灵魂的神级演技，将片中盲人钢琴家的孤独脆弱、坚韧顽强完美演绎，并凭借这个角色再摘影帝桂冠，成为万众瞩目的三冠影帝。此前，该片确认引进的消息一经发布，就引发全网热议，很多观众表示已迫不及待想去电影院感受盲人钢琴家的传奇人生！",
            300,
            360,2),
    HOT_1("《扬名立万》",
            "http://192.168.1.100:8080/pc/img/hot_6.jpg",
            "",
            2210,
            6000,3),
    HOT_2("《门锁》",
            "http://192.168.1.100:8080/pc/img/hot_7.jpg",
            "",
            243,
            160,3),
    HOT_3("《铁道英雄》",
            "http://192.168.1.100:8080/pc/img/hot_8.jpg",
            "",
            201,
            160,3),
    HOT_4("《梅艳芳》",
            "http://192.168.1.100:8080/pc/img/hot_9.jpg",
            "",
            900,
            1000,3),
    HOT_5("《从零起航》",
            "http://192.168.1.100:8080/pc/img/hot_10.jpg",
            "",
            80,
            60,3)
    ;



    public static MovieNews fromEnum(MovieNewsEnum newsEnum) {
        MovieNews news = new MovieNews();
        news.setDesc(newsEnum.getDesc());
        news.setImgSrc(newsEnum.getImgSrc());
        news.setTitle(newsEnum.getTitle());
        news.setPlayCount(newsEnum.getPlayCount());
        news.setViewCount(newsEnum.getViewCount());
        news.setType(newsEnum.getType());
        return news;
    }

    private String title;
    private String imgSrc;
    private String desc;
    private int viewCount;
    private int playCount;
    private int type;

    MovieNewsEnum(String title, String imgSrc, String desc, int viewCount, int playCount,int type) {
        this.title = title;
        this.imgSrc = imgSrc;
        this.desc = desc;
        this.viewCount = viewCount;
        this.playCount = playCount;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public String getDesc() {
        return desc;
    }

    public int getViewCount() {
        return viewCount;
    }

    public int getPlayCount() {
        return playCount;
    }

    public int getType() {
        return type;
    }
}
