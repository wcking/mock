package com.wcking.mockserver.entity;

import lombok.Data;

@Data
public class MovieData {

    private int id;
    private String movieName;
    private String imgSrc;
    private float score;
    private String date;
    private int booking;
}
