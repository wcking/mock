package com.wcking.mockserver.entity;

import lombok.Data;

@Data
public class LoginBody {

    private String loginName;
    private String pwd;
}
