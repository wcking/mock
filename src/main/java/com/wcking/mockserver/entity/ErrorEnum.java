package com.wcking.mockserver.entity;

public enum ErrorEnum {

    PARAM_ERROR("100", "参数错误"),
    NO_USER_ERROR("101", "没有该用户"),
    NO_LOGIN_ERROR("102", "没有登录"),
    PWD_ERROR("103", "密码错误"),
    NO_MOVIE_ERROR("104", "没有该电影");

    private String code;
    private String msg;

    private ErrorEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
