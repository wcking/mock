package com.wcking.mockserver.entity;

import lombok.Data;

@Data
public class BaseResult<T> {

    private String code;
    private String msg;
    private T data;

    public static BaseResult fail(String code, String msg) {
        BaseResult r = new BaseResult();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }

    public static BaseResult fail(ErrorEnum errorEnum) {
        BaseResult r = new BaseResult();
        r.setCode(errorEnum.getCode());
        r.setMsg(errorEnum.getMsg());
        return r;
    }

    public static <T> BaseResult success(T data) {
        BaseResult r = new BaseResult();
        r.setData(data);
        r.setCode("0");
        r.setMsg("success");
        return r;
    }

}
