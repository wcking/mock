package com.wcking.mockserver.entity;

import lombok.Data;

@Data
public class BillData implements  Comparable{
    private String billDate;
    private String billType;
    private int billMoney;
    private int paymentType;

    @Override
    public int compareTo(Object o) {
        BillData t = (BillData) o;
        return this.billDate.compareTo( t.getBillDate());
    }
}
