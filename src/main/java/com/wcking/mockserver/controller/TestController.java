package com.wcking.mockserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/test")
@Api(tags = "test接口管理")
public class TestController {

    @ApiOperation(value = "测试")
    @GetMapping("/test")
    public String test(@RequestParam("p") String p){
        return "hello";
    }
}
