package com.wcking.mockserver.dao;

import com.wcking.mockserver.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    public User findByLogin(@Param("loginName") String loginName);
}
