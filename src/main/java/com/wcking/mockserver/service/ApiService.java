package com.wcking.mockserver.service;

import com.wcking.mockserver.entity.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ApiService {


    private static final MovieEnum[] HOT = {MovieEnum.H_M_2,MovieEnum.H_M_3,MovieEnum.H_M_4,MovieEnum.H_M_1,
            MovieEnum.H_M_5,MovieEnum.H_M_6,MovieEnum.H_M_7,MovieEnum.H_M_8};
    private static final MovieEnum[] RECENT = {MovieEnum.R_M_2,MovieEnum.R_M_3,MovieEnum.R_M_4,MovieEnum.R_M_1,
            MovieEnum.R_M_5,MovieEnum.R_M_6,MovieEnum.R_M_7,MovieEnum.R_M_8};
    private static final MovieEnum[] TODAY = {MovieEnum.T_M_2,MovieEnum.T_M_3,MovieEnum.T_M_4,MovieEnum.T_M_1,
            MovieEnum.T_M_5,MovieEnum.T_M_6,MovieEnum.T_M_7,MovieEnum.T_M_8};
    private static final MovieEnum[] MOST = {MovieEnum.M_M_2,MovieEnum.M_M_3,MovieEnum.M_M_4,MovieEnum.M_M_1,
            MovieEnum.M_M_5,MovieEnum.M_M_6,MovieEnum.M_M_7,MovieEnum.M_M_8};

    private static final MovieEnum[] PC_REC = {MovieEnum.PC_REC_M_1,MovieEnum.PC_REC_M_2,MovieEnum.PC_REC_M_3,MovieEnum.PC_REC_M_4,
            MovieEnum.PC_REC_M_5,MovieEnum.PC_REC_M_6,MovieEnum.PC_REC_M_7,MovieEnum.PC_REC_M_8,MovieEnum.PC_REC_M_9,
            MovieEnum.PC_REC_M_10,MovieEnum.PC_REC_M_11,MovieEnum.PC_REC_M_12,MovieEnum.PC_REC_M_13,MovieEnum.PC_REC_M_14,
            MovieEnum.PC_REC_M_15,MovieEnum.PC_REC_M_16,MovieEnum.PC_REC_M_17,MovieEnum.PC_REC_M_18,MovieEnum.PC_REC_M_19,
            MovieEnum.PC_REC_M_20,MovieEnum.PC_REC_M_21,MovieEnum.PC_REC_M_22,MovieEnum.PC_REC_M_23,MovieEnum.PC_REC_M_24,
            MovieEnum.PC_REC_M_25,MovieEnum.PC_REC_M_26,MovieEnum.PC_REC_M_27,MovieEnum.PC_REC_M_28,MovieEnum.PC_REC_M_29,
            MovieEnum.PC_REC_M_30,MovieEnum.PC_REC_M_31,MovieEnum.PC_REC_M_32,MovieEnum.PC_REC_M_33,MovieEnum.PC_REC_M_34};

    private static final MovieEnum[] PC_RANK = {MovieEnum.PC_RANK_M_1,MovieEnum.PC_RANK_M_2,MovieEnum.PC_RANK_M_3,MovieEnum.PC_RANK_M_4,
            MovieEnum.PC_RANK_M_5,MovieEnum.PC_RANK_M_6,MovieEnum.PC_RANK_M_7,MovieEnum.PC_RANK_M_8,MovieEnum.PC_RANK_M_9,MovieEnum.PC_RANK_M_10};

    private static final MovieNewsEnum[] TOP_NEWS = {MovieNewsEnum.TOP_1,MovieNewsEnum.TOP_2,MovieNewsEnum.TOP_3};
    private static final MovieNewsEnum[] BOTTOM_NEWS = {MovieNewsEnum.BOTTOM_1,MovieNewsEnum.BOTTOM_2};

    private static final MovieNewsEnum[] HOT_NEWS = {MovieNewsEnum.HOT_1,MovieNewsEnum.HOT_2,MovieNewsEnum.HOT_3,
            MovieNewsEnum.HOT_4,MovieNewsEnum.HOT_5
    };

    private static final String[] TAGS = {"平原上的火焰","袁弘","周冬雨","刘昊然","程马","宋小姐","刁亦男","陈明昊","张集"};



    public static Map<String, List<MovieData>> movieMap = new HashMap<>();
    public static Map<Integer, TicketData> ticketMap = new HashMap<>();
    public static Map<String, List<MovieNews>> newMap = new HashMap<>();


    static {
        initTicketData();
        initMovieData();
        initMovieNews();
    }
    public void getPageList() {

    }

    public TicketData getTicket() {
        Random r = new Random();
        int movieId = 10000 + r.nextInt(10);
        return ticketMap.get(movieId);
    }

    public List<MovieData> getMovie(String type) {
        return movieMap.get(type);
    }

    public List<MovieNews> getNews(String type) {
        return newMap.get(type);
    }


    /**
     * 10部电影的票信息
     */
    public static void initTicketData() {
        Random random = new Random(1);
        for (int i = 10001; i <= 10010; i++) {
            TicketData ticket = new TicketData();
            ticket.setMovieId(i);
            ticket.setCellNums(40);
            ticket.setRolNums(50);
            List<Map<String,String >> soldLists = new ArrayList<>();
            int soldNums = random.nextInt(10);
            Set<String> soldSet = new HashSet<>();
            for (int j = 0; j < soldNums; j++) {
                int cell = random.nextInt(40);
                int rol = random.nextInt(50);
                String str = cell + "," + rol;
                if (soldSet.contains(str)) {
                    continue;
                }
                soldSet.add(str);
            }
            for (String s : soldSet) {
                Map<String, String> soldTicket = new HashMap<>();
                String[] ss = s.split(",");
                soldTicket.put("cell",ss[0]);
                soldTicket.put("rol", ss[1]);
                soldLists.add(soldTicket);
            }
            ticket.setSoldList(soldLists);
            ticketMap.put(i,ticket);
        }
    }

    public static void initMovieData() {
        List<MovieData> hotList = new ArrayList<>();
        List<MovieData> recentList = new ArrayList<>();
        List<MovieData> todayList = new ArrayList<>();
        List<MovieData> mostList = new ArrayList<>();

        List<MovieData> pcRecList = new ArrayList<>();
        List<MovieData> pcRankList = new ArrayList<>();

        movieMap.put("hot", hotList);
        movieMap.put("recent", recentList);
        movieMap.put("today", todayList);
        movieMap.put("most", mostList);
        movieMap.put("pcRec", pcRecList);
        movieMap.put("pcRank", pcRankList);

        for (int i = 0; i < HOT.length; i++) {
            hotList.add(MovieEnum.fromEnum(HOT[i]));
        }
        for (int i = 0; i < RECENT.length; i++) {
            recentList.add(MovieEnum.fromEnum(RECENT[i]));
        }
        for (int i = 0; i < TODAY.length; i++) {
            todayList.add(MovieEnum.fromEnum(TODAY[i]));
        }
        for (int i = 0; i < MOST.length; i++) {
            mostList.add(MovieEnum.fromEnum(MOST[i]));
        }

        for (int i = 0; i < PC_REC.length; i++) {
            pcRecList.add(MovieEnum.fromEnum(PC_REC[i]));
        }

        for (int i = 0; i < PC_RANK.length; i++) {
            pcRankList.add(MovieEnum.fromEnum(PC_RANK[i]));
        }


    }

    public static void initMovieNews(){
        List<MovieNews> top = new ArrayList<>();
        List<MovieNews> bottom = new ArrayList<>();
        List<MovieNews> hot = new ArrayList<>();
        newMap.put("top", top);
        newMap.put("bottom", bottom);
        newMap.put("hot", hot);


        for (int i =0; i < TOP_NEWS.length;i++) {
            top.add(MovieNewsEnum.fromEnum(TOP_NEWS[i]));
        }

        for (int i =0; i < BOTTOM_NEWS.length;i++) {
            MovieNews news = MovieNewsEnum.fromEnum(BOTTOM_NEWS[i]);
            if (i == 0) {
                news.setTags(Arrays.asList(TAGS));
            } else {
                news.setTags(Arrays.asList("盲琴师"));
            }
            bottom.add(news);
        }

        for (int i =0; i < HOT_NEWS.length;i++) {
            hot.add(MovieNewsEnum.fromEnum(HOT_NEWS[i]));
        }


    }
}
