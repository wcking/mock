package com.wcking.mockserver.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TicketData {

    private int movieId;
    private int rolNums;
    private int cellNums;
    private List<Map<String,String>> soldList;
}
